#!/usr/bin/env python3

import os
import subprocess

SHELL_PATH = "./shell"

class Shell:
    def __init__(self, path):
        self._path = path

    def feed(self, string):
        shell = subprocess.Popen(self._path, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE, encoding="utf8")
        stdout, stderr = shell.communicate(string + "\n", timeout=3)
        if "Sanitizer" in stderr:
            raise AssertionError("Sanitizer error:\n{}".format(stderr))
        assert shell.returncode == 0
        return stdout.replace("$ ", "").strip()

def test_shell_basics():
    shell = Shell(SHELL_PATH)

    assert not shell.feed("")
    assert shell.feed("echo hello") == "hello"
    assert shell.feed(" echo    hello  world") == "hello world"
    assert shell.feed("echo hello\necho world") == "hello\nworld"

    assert not shell.feed("touch ./foo")
    assert os.path.exists("./foo")
    assert not shell.feed("rm ./foo")
    assert not os.path.exists("./foo")

    assert shell.feed("touch ./bar\ncat ./bar\nwc -c bar\nrm ./bar") == "0 bar"
    assert not os.path.exists("./bar")

    assert shell.feed("cat\nhello\nworld") == "hello\nworld"
    assert shell.feed("./shell\necho hi\n./shell\necho hello") == "hi\nhello"

    assert shell.feed("cat /sys/proc/foo/bar") == ""
    assert shell.feed("foobar") == "Command not found"

if __name__ == "__main__":
    test_shell_basics()
