#pragma once

// Your code from atomic here.

struct spinlock {
	// Your code here.
};

static inline void spinlock_init(struct spinlock *s)
{
	// Your code here.
}

static inline void spinlock_lock(struct spinlock *s)
{
	// Your code here.
}

static inline void spinlock_unlock(struct spinlock *s)
{
	// Your code here.
}

