#include "cli.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char help_message[] = \
"Unbreakable Encryption of Files App.\n"
"\n"
"Usage: uefa [-d] <codec>\n"
"\n"
"Options:\n"
"  -h, --help          Show this message and exit.\n"
"  -d, --decrypt       Decrypt stream instead of encrypting it.\n"
;

struct cli_args cli_parse(int argc, char *argv[])
{
    struct cli_args args;
    args.codec = NULL;
    args.encrypt = true;
    for (int i = 1; i < argc; ++i) {
        char *a = argv[i];
        if (strcmp(a, "--help") == 0 || strcmp(a, "-h") == 0) {
            printf("%s", help_message);
            exit(0);
        } else if (strcmp(a, "--decrypt") == 0 || strcmp(a, "-d") == 0) {
            args.encrypt = false;
        } else if (args.codec == NULL) {
            args.codec = a;
        } else {
            printf("Failed to parse argument: '%s'\nUse '--help' for usage.\n", a);
            exit(1);
        }
    }
    if (args.codec == NULL) {
        printf("No codec specified.\n");
        exit(1);
    }
    return args;
}
