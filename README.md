```mermaid
stateDiagram-v2
    UNUSED --> EMBRYO : allocproc()
    EMBRYO --> RUNNABLE : userinit(), fork()
    RUNNABLE --> RUNNING : scheduler()
    RUNNING --> ZOMBIE : exit(), trap()
    ZOMBIE --> UNUSED : wait()
    RUNNING --> RUNNABLE : yield(), trap()
    RUNNING --> SLEEPING : sleep()
    SLEEPING --> RUNNABLE : wakeup(), kill(), trap()
```

    UNUSED --> EMBRYO : allocproc() (захват ячейки ptable)
    EMBRYO --> RUNNABLE : userinit(), fork()
    RUNNABLE --> RUNNING : scheduler()
    RUNNING --> ZOMBIE : exit()
    ZOMBIE --> UNUSED : wait()
    RUNNING --> RUNNABLE: yield(), trap() вызывает yield() по прерыванию таймера
    RUNNING --> SLEEPING: sleep()
    SLEEPING --> RUNNABLE: wakeup(), trap() вызывает wakeup() на переменной таймера
    SLEEPING --> RUNNABLE: kill() выставляет killed = 1, trap() выставляет killed = 1 при исключениях, trap() if (killed) exit()

UNUSED, EMBRYO, SLEEPING, RUNNABLE, RUNNING, ZOMBIE
allocproc(): UNUSED -> EMBRYO (захват ячейки ptable)
userinit(), fork(): EMBRYO -> RUNNABLE
scheduler(): RUNNABLE -> RUNNING
exit(): RUNNING -> ZOMBIE
wait(): ZOMBIE -> UNUSED
yield(): RUNNING -> RUNNABLE; trap(): вызывает yield() по прерыванию таймера
sleep(): RUNNING -> SLEEPING
wakeup(): SLEEPING -> RUNNABLE; trap(): вызывает wakeup() на переменной таймера
kill(): killed = 1 + SLEEPING -> RUNNABLE, trap(): killed = 1 при исключениях; trap(): if (killed) exit();